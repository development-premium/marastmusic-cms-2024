$(document).ready(function(){    

    $('.delete-link').click(function(e) {    
        e.preventDefault();
        if (confirm('Opravdu smazat ?')) {
            e.preventDefault();
            $(this).closest('.grid-container').remove();
            var deleteUrl = $(this).data('delete-url');
            $.ajax({
                type: 'POST',
                url: deleteUrl,
                success: function(response) {
                    console.log('Item deleted');
                },
                error: function(error) {
                    console.error('Error deleting item: ' + error.statusText);
                }
            });
        }
    });
    
    $('#summernote').summernote({
        height: 350,
        callbacks: {
            onImageUpload: function(image) {
                uploadImage(image[0]);
            }
        }
    });

    $('form[name="content_form"]').on('submit', function(e) {
        var summernoteContent = $('#summernote').summernote('code').trim();
        if(summernoteContent === '' || summernoteContent === '<p><br></p>') {
            alert('Nelze uložit bez obsahu');
            e.preventDefault();
        } 
    });
    
    function uploadImage(image) {
        var data = new FormData();        
        data.append("image", image);
        $.ajax({
            type: 'post',
            data: data,
            url: '/save-form-img',
            cache: false,
            contentType: false,
            processData: false,            
            success: function(response) {
                console.log(response);
                var image = $('<img>').attr('src', 'https://' + location.host + '/images/cms-images/' + response);
                $('#summernote').summernote("insertNode", image[0]);
            },
            error: function(data) {
                console.log(data);
            }
        });
    }

    function croppie(element, veiwportWidth, viewportHeight, boundaryWidth, boundaryHeight) {
        return element.croppie({
            enableExif: true,
            viewport: {
                width: veiwportWidth,
                height: viewportHeight
            },
            boundary: {
                width: boundaryWidth,
                height: boundaryHeight
            }
        });
    }

    if (typeof type !== 'undefined' && type === 'novinka') {
        $croppieMain = croppie($('#croppie-main'), 200, 132, 500, 400);        
    } else {
        $croppieMain = croppie($('#croppie-main'), 310, 440, 500, 600);
    }

    $croppieSearch = croppie($('#croppie-search'), 420, 222, 500, 300);
    $croppieDetail = croppie($('#croppie-detail'), 1366, 500, 1400, 600);
       
    function readFile(element, croppie, type) {
        element.on('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                croppie.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    console.log(type);
                });
            }
            reader.readAsDataURL(this.files[0]);
        });
    }

    $('#croppie-btn-main').click(function() {
        $('#modal-main').addClass('c-croppie__modal--visible');
    });
    $('#croppie-btn-search').click(function() {
        $('#modal-search').addClass('c-croppie__modal--visible');
    });
    $('#croppie-btn-detail').click(function() {
        $('#modal-detail').addClass('c-croppie__modal--visible');
    });

    readFile($('#content_form_image_main, #event_form_image_main'), $croppieMain, 'main');
    readFile($('#content_form_image_search, #event_form_image_search'), $croppieSearch, 'search');
    readFile($('#content_form_image_detail, #event_form_image_detail'), $croppieDetail, 'detail');

    function uploadImage(element, croppieType, previewElement, dataInput) {
        element.on('click', function(ev) {   
            var folder = $('.active').data('folder'),            
                dataType = $('.active').data('type'),
                url = '/save-img/' + folder;
            croppieType.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function(response) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        "image": response,
                        "type": dataType
                    },
                    success: function(data) {
                        html = '<img src="' + response + '" />';
                        previewElement.html(html);
                        dataInput.attr('value', data); 
                    }
                });
                $('.c-croppie__modal').removeClass('c-croppie__modal--visible');
                previewElement.addClass('c-croppie__preview--visible')
            });
        });
    }

    uploadImage($('#croppie-upload-main'), $croppieMain, $("#upload-main-preview"), $("#content_form_image"));
    uploadImage($('#croppie-upload-search'), $croppieSearch, $("#upload-search-preview"), $("#content_form_image_reference"));
    uploadImage($('#croppie-upload-detail'), $croppieDetail, $("#upload-detail-preview"), $("#content_form_img_main"));

    $('#content_form_publish_date').datetimepicker();

    $('.datetimepicker-minutes').click(function() {
        $('.datetimepicker').hide();
    });

    $('#gallery_form_images').change(function() {
        $(this).siblings('label').text('Nahráno: ' + ($(this).get(0).files.length));
    });

    if (typeof imageType !== 'undefined' && imageType === 'galerie') {

        $('#croppie-btn').click(function() {
            $('#modal').addClass('c-croppie__modal--visible');
        });

        $croppieGallery = croppie($('#croppie'), 200, 132, 500, 400);        
    
        $('#gallery_form_img').on('change', function () { 
            var reader = new FileReader();
            reader.onload = function (e) {
                $croppieGallery.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    console.log('galerie');
                });
            }
            reader.readAsDataURL(this.files[0]);
        });
    
        $('#croppie-upload').on('click', function(ev) {   
            $croppieGallery.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function(response) {
                $.ajax({
                    url: "/save-img/galerie",
                    type: "POST",
                    data: {
                        "image": response
                    },
                    success: function(data) {
                        html = '<img src="' + response + '" />';
                        $("#upload-preview").html(html);
                        $("#gallery_form_image").attr('value', data); 
                    }
                });
                $('.c-croppie__modal').removeClass('c-croppie__modal--visible');
                $('#upload-preview').addClass('c-croppie__preview--visible')
            });
        }); 
    }
});
