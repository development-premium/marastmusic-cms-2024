var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('sass', function() {
	return gulp.src('assets/sass/main.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('public/css'));
});

gulp.task('scripts', function() {
	return gulp.src([
		'assets/js/cms.js'
	])
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest('public/js'));
});

gulp.task('intro-script', function() {
	return gulp.src([
		'assets/js/intro.js'
	])
		.pipe(concat('intro.js'))
		.pipe(uglify())
		.pipe(gulp.dest('public/js'));
});

gulp.task('watch', function() {
	gulp.watch('assets/sass/*.scss', gulp.series('sass'));
	gulp.watch('assets/js/*.js', gulp.series('scripts', 'intro-script'));
});