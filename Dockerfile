FROM php:8.1-apache

RUN docker-php-ext-install pdo_mysql

RUN a2enmod rewrite

COPY . /var/www/html

WORKDIR /var/www/html

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN composer install --no-interaction

RUN chown -R www-data:www-data /var/www/html/var

RUN apt-get update && apt-get install -y nano

RUN a2enmod mime