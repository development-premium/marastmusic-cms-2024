<?php

namespace App\Repository;

use App\Entity\Interviews;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class InterviewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Interviews::class);
    }

    public function getCmsListQueryBuilder($search = null)
    {
        $qb = $this->createQueryBuilder('q');

        $qb->select('q.title', 'q.id', 'q.author_name', 'q.added', 'q.publish_date')
        ->orderBy('q.added', 'DESC');

        if ($search) {
            $qb->andWhere('q.title LIKE :search OR q.author_name LIKE :search')
               ->setParameter('search', '%' . $search . '%');
        }

        return $qb;
    }

    public function findInterviewsBoard(int $boxCount): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
    
        $queryBuilder
            ->select('a.id, a.author_name, a.comments_count, a.type, a.title, a.publish_date, a.image_reference, a.url_path')
            ->from('App\Entity\Interviews', 'a')
            ->andWhere('a.public = :public')
            ->setParameter('public', 1)
            ->orderBy('a.publish_date', 'DESC')
            ->setFirstResult($boxCount)
            ->setMaxResults(9);
    
        return $queryBuilder->getQuery()->getResult();
    }

    public function findInterviews(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT 
                id, 
                comments_count, 
                author_name, 
                url_path, 
                publish_date, 
                title, 
                image_reference, 
                img_main,
                type,
                is_redesign_version
            FROM interviews 
            WHERE publish_date < NOW() AND public = 1
            ORDER BY publish_date DESC 
            LIMIT 4
        ';

        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery();

        return $result->fetchAllAssociative();
    }

    public function incrementCommentsCountForInterviews(string $url): int
    {
        $qb = $this->createQueryBuilder('r')
            ->update('App\Entity\Interviews', 'r')
            ->set('r.comments_count', 'r.comments_count + 1')
            ->where('r.url_path LIKE :url')
            ->setParameter('url', '%' . $url . '%');

        return $qb->getQuery()->execute();
    }
}
