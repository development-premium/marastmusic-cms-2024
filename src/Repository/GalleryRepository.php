<?php

namespace App\Repository;

use App\Entity\Gallery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class GalleryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gallery::class);
    }

    public function findLastTwoGalleries()
    {
        return $this->createQueryBuilder('g')
            ->orderBy('g.id', 'DESC')
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();
    }

    public function findNumberOfPhotos(string $url_path): int
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('COUNT(i.id)')
            ->from(Gallery::class, 'i')
            ->where('i.url_path LIKE :url_path')
            ->setParameter('url_path', "%$url_path%")
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findGalleryData(string $galleryName): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('i.title, i.contentobject_id, i.related_content, i.author_name, i.event_date, i.venue')
            ->from(Gallery::class, 'i')
            ->where('i.url_path LIKE :galleryName')
            ->setParameter('galleryName', "%$galleryName%")
            ->getQuery()
            ->getResult()[0];
    }

    public function getGalleryBoard(int $boxCount, int $limit): array
    {
        $queryBuilder = $this->createQueryBuilder('g')
            ->select('g.id, g.author_name, g.comments_count, g.publish_date, g.title, g.image_reference, g.url_path')
            ->orderBy('g.publish_date', 'DESC')
            ->setFirstResult($boxCount)
            ->setMaxResults($limit);

        return $queryBuilder->getQuery()->getResult();
    }
}
