<?php

namespace App\Repository;

use App\Entity\Reviews;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ReviewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reviews::class);
    }

    public function getCmsListQueryBuilder($search = null)
    {
        $qb = $this->createQueryBuilder('q');

        $qb->select('q.title', 'q.id', 'q.author_name', 'q.added', 'q.publish_date')
        ->orderBy('q.added', 'DESC');

        if ($search) {
            $qb->andWhere('q.title LIKE :search OR q.author_name LIKE :search')
               ->setParameter('search', '%' . $search . '%');
        }

        return $qb;
    }

    public function incrementCommentsCountForReviews(string $url): int
    {
        $qb = $this->createQueryBuilder('r')
            ->update('App\Entity\Reviews', 'r')
            ->set('r.comments_count', 'r.comments_count + 1')
            ->where('r.url_path LIKE :url')
            ->setParameter('url', '%' . $url . '%');

        return $qb->getQuery()->execute();
    }

    public function findReviewsBoard(int $boxCount): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
    
        $queryBuilder
            ->select('a.id, a.author_name, a.comments_count, a.title, a.type, a.publish_date, a.image_reference, a.url_path')
            ->from('App\Entity\Reviews', 'a')
            ->andWhere('a.public = :public')
            ->setParameter('public', 1)
            ->orderBy('a.publish_date', 'DESC')
            ->setFirstResult($boxCount)
            ->setMaxResults(9);
            
        return $queryBuilder->getQuery()->getResult();
    }

    public function findReviews(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT 
                id, 
                comments_count, 
                author_name, 
                url_path, 
                publish_date, 
                band, 
                album, 
                image_reference, 
                img_main,
                type,
                title,
                is_redesign_version
            FROM reviews
            WHERE public = 1
            ORDER BY publish_date DESC
            LIMIT 4
        ';

        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery();

        return $result->fetchAllAssociative();
    }

    public function findReviewDetailData(string $urlpath): array 
    {
        $qb = $this->createQueryBuilder('r')
            ->select('r.rating, r.body, r.tags, r.related_content, r.band, r.album, r.contentobject_id, r.title, r.img_main, r.author_name, r.intro, r.publish_date, r.type, r.url_path, r.is_redesign_version, r.slider')
            ->where('r.url_path = :urlpath')
            ->setParameter('urlpath', $urlpath);            

        return $qb->getQuery()->getResult()[0];
    }    
}
