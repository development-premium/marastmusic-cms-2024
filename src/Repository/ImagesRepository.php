<?php

namespace App\Repository;

use App\Entity\Images;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ImagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Images::class);
    }

    public function findImages(string $galleryName): array
    {
        return $this->createQueryBuilder('i')
            ->select('i.image_reference', 'i.name')
            ->where('i.url_path LIKE :galleryName')
            ->setParameter('galleryName', '%' . $galleryName . '%')
            ->getQuery()
            ->getResult();
    }
}
