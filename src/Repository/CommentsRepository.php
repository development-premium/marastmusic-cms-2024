<?php

namespace App\Repository;

use App\Entity\Comments;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CommentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comments::class);
    }

    public function findAllComments($limit = 6): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT * FROM (
                SELECT
                    id,
                    name,
                    detail_title,
                    url_path,
                    created,
                    ROW_NUMBER() OVER (PARTITION BY url_path ORDER BY created DESC) rownumber
                FROM comments
            ) AS a
            WHERE a.rownumber = 1
            ORDER BY created DESC
            LIMIT :limit
        ';

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('limit', $limit, \PDO::PARAM_INT);
        $result = $stmt->executeQuery();

        return $result->fetchAllAssociative();
    }

    public function findCommentsBasedOnType(string $type): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT * FROM (
                SELECT
                    id,
                    name,
                    detail_title,
                    url_path,
                    created,
                    ROW_NUMBER() OVER(PARTITION BY url_path ORDER BY created DESC) rownumber
                FROM comments
                WHERE type = :type
            ) AS a
            WHERE a.rownumber = 1
            ORDER BY created DESC
            LIMIT 6
        ';

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('type', $type);
        $result = $stmt->executeQuery();

        return $result->fetchAllAssociative();
    }

    public function saveComment(Comments $comment): void
    {
        try {
            $entityManager = $this->getEntityManager();
            $entityManager->persist($comment);
            $entityManager->flush();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getDetailComments(int $contentObjectId): array
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQueryBuilder()
            ->select('c.id, c.name, c.created, c.text, c.is_old')
            ->from(Comments::class, 'c')
            ->where('c.contentobject_id = :contentObjectId')
            ->setParameter('contentObjectId', $contentObjectId)
            ->orderBy('c.created', 'DESC')
            ->getQuery();

        return $query->getArrayResult();
    }
}