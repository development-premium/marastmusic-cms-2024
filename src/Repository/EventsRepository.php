<?php

namespace App\Repository;

use App\Entity\Events;
use App\Entity\UserEvents;
use Doctrine\DBAL\Types\Types;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EventsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Events::class);
    }

    public function getCmsListQueryBuilder($search = null)
    {
        $qb = $this->createQueryBuilder('q');

        $qb->select('q.title', 'q.id', 'q.added')
        ->orderBy('q.added', 'DESC');

        if ($search) {
            $qb->andWhere('q.title LIKE :search OR q.author_name LIKE :search')
               ->setParameter('search', '%' . $search . '%');
        }

        return $qb;
    }

    public function findEvents(): array
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.public = :public')
            ->andWhere('e.date > :currentDate')
            ->setParameter('public', 1)
            ->setParameter('currentDate', new \DateTime(), Types::DATETIME_MUTABLE)
            ->orderBy('e.date', 'ASC')
            ->setMaxResults(7)
            ->getQuery()
            ->getResult();
    }

    public function save(UserEvents $event): void
    {
        $this->_em->persist($event);
        $this->_em->flush();
    }

    public function findEventData(string $url_path, string $type): array
    {
        $urlPathFull = $type . "/" . $url_path;

        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('e.title', 'e.type', 'e.tags', 'e.contentobject_id', 'e.bands', 'e.image', 'e.venue', 'e.date', 'e.time', 'e.info', 'e.text', 'e.facebook', 'e.multiday_string', 'e.event_type', 'e.related_content')
            ->from(Events::class, 'e')
            ->where('e.url_path = :urlPath')
            ->andWhere('e.public = :public')
            ->setParameter('urlPath', $urlPathFull)
            ->setParameter('public', 1);

        return $queryBuilder->getQuery()->getResult()[0];
    }

    public function getEventsBoard(int $boxCount): array 
    {
        return $this->createQueryBuilder('e')
            ->select('e.id, e.comments_count, e.title, e.venue, e.date, e.image_reference, e.url_path, e.multiday_string')
            ->where('e.date > CURRENT_DATE()')
            ->andWhere('e.public = :public')
            ->setParameter('public', 1)
            ->orderBy('e.date', 'ASC')
            ->setMaxResults(8)
            ->setFirstResult($boxCount)
            ->getQuery()
            ->getResult();
    }
}
