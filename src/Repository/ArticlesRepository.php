<?php

namespace App\Repository;

use App\Entity\Articles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ArticlesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Articles::class);
    }

    public function getCmsListQueryBuilder($search = null)
    {
        $qb = $this->createQueryBuilder('q');

        $qb->select('q.title', 'q.id', 'q.author_name', 'q.added', 'q.publish_date')
        ->orderBy('q.added', 'DESC');

        if ($search) {
            $qb->andWhere('q.title LIKE :search OR q.author_name LIKE :search')
               ->setParameter('search', '%' . $search . '%');
        }

        return $qb;
    }

    public function findArticlesBoard(int $boxCount): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
    
        $queryBuilder
            ->select('a.id, a.author_name, a.comments_count, a.title, a.category, a.type, a.publish_date, a.image_reference, a.url_path')
            ->from('App\Entity\Articles', 'a')
            ->where('a.public = 1')
            ->orderBy('a.publish_date', 'DESC')
            ->setFirstResult($boxCount)
            ->setMaxResults(9);
    
        return $queryBuilder->getQuery()->getResult();
    }
    
    public function findAlbumsBoard(int $boxCount): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
    
        $queryBuilder
            ->select('a.id, a.author_name, a.comments_count, a.title, a.category, a.type, a.publish_date, a.image_reference, a.url_path')
            ->from('App\Entity\Articles', 'a')
            ->where('a.category = :category')
            ->andWhere('a.public = 1')
            ->setParameter('category', 'Alba měsíce')
            ->orderBy('a.publish_date', 'DESC')
            ->setFirstResult($boxCount)
            ->setMaxResults(9);
    
        return $queryBuilder->getQuery()->getResult();
    }
    
    public function findOthersBoard(int $boxCount): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
    
        $queryBuilder
            ->select('a.id, a.author_name, a.comments_count, a.title, a.category, a.type, a.publish_date, a.image_reference, a.url_path')
            ->from('App\Entity\Articles', 'a')
            ->where('a.category != :category')
            ->andWhere('a.public = 1')
            ->setParameter('category', 'Alba měsíce')
            ->orderBy('a.publish_date', 'DESC')
            ->setFirstResult($boxCount)
            ->setMaxResults(9);
    
        return $queryBuilder->getQuery()->getResult();
    }

    public function findAllArticles(int $limit = 4): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT id, comments_count, author_name, url_path, publish_date, title, img_main, image_reference, is_redesign_version, type FROM reports
            WHERE public = 1
            UNION All
            SELECT id, comments_count, author_name, url_path, publish_date, title, img_main, image_reference, is_redesign_version, type FROM reviews
            WHERE public = 1
            UNION All
            SELECT id, comments_count, author_name, url_path, publish_date, title, img_main, image_reference, is_redesign_version, type FROM interviews
            WHERE public = 1
            UNION All
            SELECT id, comments_count, author_name, url_path, publish_date, title, img_main, image_reference, is_redesign_version, type FROM articles
            WHERE publish_date < NOW() AND public = 1
            ORDER BY publish_date DESC LIMIT :limit
        ';

        $stmt = $conn->prepare($sql);        
        $stmt->bindParam(':limit', $limit, \PDO::PARAM_INT);
        $result = $stmt->executeQuery();

        return $result->fetchAllAssociative();
    }

    public function findOthers(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT 
                id, 
                comments_count, 
                author_name, 
                url_path, 
                publish_date, 
                title, 
                image_reference, 
                img_main,
                category,
                is_redesign_version
            FROM articles
            WHERE NOT category = "Alba měsíce" AND public = 1
            ORDER BY publish_date DESC
            LIMIT 4
        ';

        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery();

        return $result->fetchAllAssociative();
    }

    public function findAlbums(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT
                id,
                url_path,
                comments_count,
                author_name,
                publish_date,
                title,
                image_reference,
                img_main,
                is_redesign_version,
                category
            FROM articles
            WHERE category = "Alba měsíce" AND public = 1
            ORDER BY publish_date DESC
            LIMIT 4
        ';

        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery();

        return $result->fetchAllAssociative();
    }

    public function findDetailData(string $urlpath): array
    {
        $entityManager = $this->getEntityManager();
    
        $connection = $entityManager->getConnection();
    
        $sql = "
            SELECT a.id, a.tags, a.related_content, a.comments_count, a.url_path, a.body, a.intro, a.contentobject_id, a.author_name, a.publish_date, a.title, a.type, a.img_main, a.slider
            FROM articles a
            WHERE a.url_path = :urlpath
            UNION
            SELECT i.id, i.tags, i.related_content, i.comments_count, i.url_path, i.body, i.intro, i.contentobject_id, i.author_name, i.publish_date, i.title, i.type, i.img_main, i.slider
            FROM interviews i
            WHERE i.url_path = :urlpath
            UNION
            SELECT r.id, r.tags, r.related_content, r.comments_count, r.url_path, r.body, r.intro, r.contentobject_id, r.author_name, r.publish_date, r.title, r.type, r.img_main, r.slider
            FROM reports r
            WHERE r.url_path = :urlpath
        ";
    
        $stmt = $connection->executeQuery($sql, ['urlpath' => $urlpath]);
    
        return $stmt->fetchAllAssociative()[0];
    }

    public function incrementCommentsCountForArticles(string $url): int
    {
        $qb = $this->createQueryBuilder('r')
            ->update('App\Entity\Articles', 'r')
            ->set('r.comments_count', 'r.comments_count + 1')
            ->where('r.url_path LIKE :url')
            ->setParameter('url', '%' . $url . '%');

        return $qb->getQuery()->execute();
    }
}
