<?php

namespace App\Entity;

use App\Repository\ReportsRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReportsRepository::class)]
class Reports
{
    #[ORM\Id, ORM\GeneratedValue, ORM\Column(type: "integer")]
    private int $id;

    #[ORM\Column(type: "bigint")]
    private int $contentobject_id;

    #[ORM\Column(type: "integer", nullable: true)]
    private int $creator_id;

    #[ORM\Column(type: "string", length: 255)]
    private string $url_path;

    #[ORM\Column(type: "string", length: 255)]
    private string $title;

    #[ORM\Column(type: "string", length: 255)]
    private string $author_name;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private string $author_email;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private string $category;

    #[ORM\Column(type: "text", nullable: true)]
    private string $image;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private string $event_date;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private string $venue;

    #[ORM\Column(type: "text", nullable: true)]
    private string $intro;

    #[ORM\Column(type: "text")]
    private string $body;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $publish_date = null;

    #[ORM\Column(type: "string", length: 255)]
    private string $type;

    #[ORM\Column(type: "integer")]
    private int $comments_count = 0;

    #[ORM\Column(type: "text", nullable: true)]
    private string $tags;

    #[ORM\Column(type: "text", nullable: true)]
    private string $image_reference;

    #[ORM\Column(type: "string", length: 255)]
    private string $related_content;

    #[ORM\Column(type: "integer")]
    private int $views = 0;

    #[ORM\Column(type: "integer")]
    private int $is_redesign_version;

    #[ORM\Column(type: "string", length: 255)]
    private string $img_main;

    #[ORM\Column(type: "boolean")]
    private bool $public;

    #[ORM\Column(type: "text", nullable: true)]
    private string $slider;

    #[ORM\Column(type: "string", length: 255)]
    private string $added;

    public function incrementViews()
    {
        $this->views++;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    
    public function getContentobjectId()
    {
        return $this->contentobject_id;
    }
    
    public function setContentobjectId($contentobject_id)
    {
        $this->contentobject_id = $contentobject_id;
        return $this;
    }
    
    public function getCreatorId()
    {
        return $this->creator_id;
    }
    
    public function setCreatorId($creator_id)
    {
        $this->creator_id = $creator_id;
        return $this;
    }
    
    public function getUrlPath()
    {
        return $this->url_path;
    }
    
    public function setUrlPath($url_path)
    {
        $this->url_path = $url_path;
        return $this;
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    
    public function getAuthorName()
    {
        return $this->author_name;
    }
    
    public function setAuthorName($author_name)
    {
        $this->author_name = $author_name;
        return $this;
    }
    
    public function getAuthorEmail()
    {
        return $this->author_email;
    }
    
    public function setAuthorEmail($author_email)
    {
        $this->author_email = $author_email;
        return $this;
    }
    
    public function getCategory()
    {
        return $this->category;
    }
    
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }
    
    public function getImage()
    {
        return $this->image;
    }
    
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }
    
    public function getEvent_date()
    {
        return $this->event_date;
    }
    
    public function setEvent_date($event_date)
    {
        $this->event_date = $event_date;
        return $this;
    }
    
    public function getVenue()
    {
        return $this->venue;
    }
    
    public function setVenue($venue)
    {
        $this->venue = $venue;
        return $this;
    }
    
    public function getIntro()
    {
        return $this->intro;
    }
    
    public function setIntro($intro)
    {
        $this->intro = $intro;
        return $this;
    }
    
    public function getBody()
    {
        return $this->body;
    }
    
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }
    
    public function getPublishDate()
    {
        return $this->publish_date;
    }
    
    public function setPublishDate($publish_date)
    {
        $this->publish_date = $publish_date;
        return $this;
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    
    public function getComments_count()
    {
        return $this->comments_count;
    }
    
    public function setComments_count($comments_count)
    {
        $this->comments_count = $comments_count;
        return $this;
    }
    
    public function getTags()
    {
        return $this->tags;
    }
    
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }
    
    public function getImageReference()
    {
        return $this->image_reference;
    }
    
    public function setImageReference($image_reference)
    {
        $this->image_reference = $image_reference;
        return $this;
    }
    
    public function getRelatedContent()
    {
        return $this->related_content;
    }
    
    public function setRelatedContent($related_content)
    {
        $this->related_content = $related_content;
        return $this;
    }
    
    public function getViews()
    {
        return $this->views;
    }
    
    public function setViews($views)
    {
        $this->views = $views;
        return $this;
    }
    
    public function getIsRedesignVersion()
    {
        return $this->is_redesign_version;
    }
    
    public function setIsRedesignVersion($is_redesign_version)
    {
        $this->is_redesign_version = $is_redesign_version;
        return $this;
    }
    
    public function getImgMain()
    {
        return $this->img_main;
    }
    
    public function setImgMain($img_main)
    {
        $this->img_main = $img_main;
        return $this;
    }
    
    public function getPublic()
    {
        return $this->public;
    }
    
    public function setPublic($public)
    {
        $this->public = $public;
        return $this;
    }
    
    public function getSlider()
    {
        return $this->slider;
    }
    
    public function setSlider($slider)
    {
        $this->slider = $slider;
        return $this;
    }
    
    public function getAdded()
    {
        return $this->added;
    }
    
    public function setAdded($added)
    {
        $this->added = $added;
        return $this;
    }    
}