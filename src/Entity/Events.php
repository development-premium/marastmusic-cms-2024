<?php

namespace App\Entity;

use App\Repository\EventsRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventsRepository::class)]
class Events
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: "integer", nullable: true)]
    private $publication_date;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private $url_path;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private $title;

    #[ORM\Column(type: "string", length: 1000, nullable: true)]
    private $bands;

    #[ORM\Column(type: "string", length: 1000, nullable: true)]
    private $image;

    #[ORM\Column(type: "string", length: 1000, nullable: true)]
    private $venue;

    #[ORM\Column(type: "datetime", nullable: true)]
    private $date;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private $time;

    #[ORM\Column(type: "string", length: 2000, nullable: true)]
    private $tags;

    #[ORM\Column(type: "text", nullable: true)]
    private $text;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private $facebook;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private $type;

    #[ORM\Column(type: "integer", nullable: true)]
    private $comments_count;

    #[ORM\Column(type: "string", length: 1000, nullable: true)]
    private $image_reference;

    #[ORM\Column(type: "integer", nullable: true)]
    private $is_redesign_version;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private $related_content;

    #[ORM\Column(type: "string", length: 255, nullable: true)]
    private $img_main;

    #[ORM\Column(type: "boolean", nullable: true)]
    private $public = true;

    #[ORM\Column(type: Types::BIGINT)]
    private ?string $contentobject_id = null;

    #[ORM\Column(length: 1000, nullable: true)]
    private ?string $info = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $multiday_string = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $event_type = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $added = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublicationDate()
    {
        return $this->publication_date;
    }

    public function setPublicationDate($publication_date)
    {
        $this->publication_date = $publication_date;
        return $this;
    }

    public function getUrlPath()
    {
        return $this->url_path;
    }

    public function setUrlPath($url_path)
    {
        $this->url_path = $url_path;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getBands()
    {
        return $this->bands;
    }

    public function setBands($bands)
    {
        $this->bands = $bands;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function getVenue()
    {
        return $this->venue;
    }

    public function setVenue($venue)
    {
        $this->venue = $venue;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function getFacebook()
    {
        return $this->facebook;
    }

    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getCommentsCount()
    {
        return $this->comments_count;
    }

    public function setCommentsCount($comments_count)
    {
        $this->comments_count = $comments_count;
        return $this;
    }

    public function getImageReference()
    {
        return $this->image_reference;
    }

    public function setImageReference($image_reference)
    {
        $this->image_reference = $image_reference;
        return $this;
    }

    public function getIsRedesignVersion()
    {
        return $this->is_redesign_version;
    }

    public function setIsRedesignVersion($is_redesign_version)
    {
        $this->is_redesign_version = $is_redesign_version;
        return $this;
    }

    public function getRelatedContent()
    {
        return $this->related_content;
    }

    public function setRelatedContent($related_content)
    {
        $this->related_content = $related_content;
        return $this;
    }

    public function getImgMain()
    {
        return $this->img_main;
    }

    public function setImgMain($img_main)
    {
        $this->img_main = $img_main;
        return $this;
    }

    public function getPublic()
    {
        return $this->public;
    }

    public function setPublic($public)
    {
        $this->public = $public;
        return $this;
    }

    public function getContentobjectId(): ?string
    {
        return $this->contentobject_id;
    }

    public function setContentobjectId(string $contentobject_id): static
    {
        $this->contentobject_id = $contentobject_id;

        return $this;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function setInfo(?string $info): static
    {
        $this->info = $info;

        return $this;
    }

    public function getMultidayString(): ?string
    {
        return $this->multiday_string;
    }

    public function setMultidayString(?string $multiday_string): static
    {
        $this->multiday_string = $multiday_string;

        return $this;
    }

    public function getEventType(): ?string
    {
        return $this->event_type;
    }

    public function setEventType(?string $event_type): static
    {
        $this->event_type = $event_type;

        return $this;
    }

    public function getAdded()
    {
        return $this->added;
    }

    public function setAdded($added)
    {
        $this->added = $added;

        return $this;
    }
}
