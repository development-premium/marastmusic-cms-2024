<?php

namespace App\Entity;

use App\Repository\ReviewsRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReviewsRepository::class)]
class Reviews
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::BIGINT)]
    private ?string $contentobject_id = null;

    #[ORM\Column(length: 255)]
    private ?string $url_path = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    private ?string $author_name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $author_email = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $body = null;

    #[ORM\Column(type: "text", nullable: true)]
    private ?string $image = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $publish_date = null;

    #[ORM\Column(type: "text", nullable: true)]
    private ?string $tags = null;

    #[ORM\Column(length: 255)]
    private ?string $type = null;

    #[ORM\Column]
    private ?int $comments_count = 0;

    #[ORM\Column(type: "text", nullable: true)]
    private ?string $image_reference = null;

    #[ORM\Column(length: 255)]
    private ?string $related_content = null;

    #[ORM\Column]
    private ?int $views = 0;

    #[ORM\Column]
    private ?int $is_redesign_version = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $intro = null;

    #[ORM\Column(length: 255)]
    private ?string $img_main = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $slider = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $public = null;

    #[ORM\Column(length: 255)]
    private ?string $added = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $rating = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $band = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $album = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $albumid = null;

    public function incrementViews()
    {
        $this->views++;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContentobjectId(): ?string
    {
        return $this->contentobject_id;
    }

    public function setContentobjectId(string $contentobject_id): static
    {
        $this->contentobject_id = $contentobject_id;

        return $this;
    }

    public function getUrlPath(): ?string
    {
        return $this->url_path;
    }

    public function setUrlPath(string $url_path): static
    {
        $this->url_path = $url_path;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthorName(): ?string
    {
        return $this->author_name;
    }

    public function setAuthorName(string $author_name): static
    {
        $this->author_name = $author_name;

        return $this;
    }

    public function getAuthorEmail(): ?string
    {
        return $this->author_email;
    }

    public function setAuthorEmail(string $author_email): static
    {
        $this->author_email = $author_email;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): static
    {
        $this->body = $body;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): static
    {
        $this->image = $image;

        return $this;
    }

    public function getPublishDate(): ?\DateTimeInterface
    {
        return $this->publish_date;
    }

    public function setPublishDate(\DateTimeInterface $publish_date): static
    {
        $this->publish_date = $publish_date;

        return $this;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function setTags(string $tags): static
    {
        $this->tags = $tags;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getCommentsCount(): ?int
    {
        return $this->comments_count;
    }

    public function setCommentsCount(int $comments_count): static
    {
        $this->comments_count = $comments_count;

        return $this;
    }

    public function getImageReference(): ?string
    {
        return $this->image_reference;
    }

    public function setImageReference(string $image_reference): static
    {
        $this->image_reference = $image_reference;

        return $this;
    }

    public function getRelatedContent(): ?string
    {
        return $this->related_content;
    }

    public function setRelatedContent(string $related_content): static
    {
        $this->related_content = $related_content;

        return $this;
    }

    public function getViews(): ?int
    {
        return $this->views;
    }

    public function setViews(int $views): static
    {
        $this->views = $views;

        return $this;
    }

    public function getIsRedesignVersion(): ?int
    {
        return $this->is_redesign_version;
    }

    public function setIsRedesignVersion(int $is_redesign_version): static
    {
        $this->is_redesign_version = $is_redesign_version;

        return $this;
    }

    public function getIntro(): ?string
    {
        return $this->intro;
    }

    public function setIntro(?string $intro): static
    {
        $this->intro = $intro;

        return $this;
    }

    public function getImgMain(): ?string
    {
        return $this->img_main;
    }

    public function setImgMain(string $img_main): static
    {
        $this->img_main = $img_main;

        return $this;
    }

    public function getSlider(): ?string
    {
        return $this->slider;
    }

    public function setSlider(?string $slider): static
    {
        $this->slider = $slider;

        return $this;
    }

    public function getPublic(): ?int
    {
        return $this->public;
    }

    public function setPublic(int $public): static
    {
        $this->public = $public;

        return $this;
    }

    public function getAdded(): ?string
    {
        return $this->added;
    }

    public function setAdded(string $added): static
    {
        $this->added = $added;

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->rating;
    }

    public function setRating(string $rating): static
    {
        $this->rating = $rating;

        return $this;
    }

    public function getBand(): ?string
    {
        return $this->band;
    }

    public function setBand(string $band): static
    {
        $this->band = $band;

        return $this;
    }

    public function getAlbum(): ?string
    {
        return $this->album;
    }

    public function setAlbum(string $album): static
    {
        $this->album = $album;

        return $this;
    }

    public function getAlbumid(): ?string
    {
        return $this->albumid;
    }

    public function setAlbumid(string $albumid): static
    {
        $this->albumid = $albumid;

        return $this;
    }
}
