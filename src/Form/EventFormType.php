<?php

namespace App\Form;

use App\DTO\EventsDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;

class EventFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class)
            ->add('text', TextareaType::class, [
                'attr' => ['rows' => 10],
                'required' => true
            ])
            ->add('image_main', FileType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('image_search', FileType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('image_detail', FileType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('image', HiddenType::class)
            ->add('image_reference', HiddenType::class)
            ->add('img_main', HiddenType::class)
            ->add('venue', TextType::class)
            ->add('date', DateType::class, [
                'required' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',  
            ])
            ->add('time', TextType::class)

            ->add('event_end', DateType::class, [
                'required' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',  
            ])

            ->add('event_type', ChoiceType::class, [
                'choices'  => [
                    'Koncert' => 'Koncert',
                    'Festival' => 'Festival',
                ]])

            ->add('tags', TextType::class);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function ($event) {
            $data = $event->getData();

            if (empty($data['image'])) {
                $data['image'] = 'images/default.jpg';
            }
    
            if (empty($data['image_reference'])) {
                $data['image_reference'] = 'images/default.jpg';
            }
    
            if (empty($data['img_main'])) {
                $data['img_main'] = 'images/default.jpg';
            }
    
            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventsDTO::class,
            'validation_groups' => ['contentValidation'],
        ]);
    }
}
