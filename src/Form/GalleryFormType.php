<?php

namespace App\Form;

use App\Entity\Gallery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GalleryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {        
        $builder
            ->add('title', TextType::class)
            ->add('venue', TextType::class)
            ->add('event_date', TextType::class)
            ->add('author_name', TextType::class)
            ->add('description', TextType::class)
            ->add('publish_date', TextType::class)
            ->add('img', FileType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('image', HiddenType::class, [
                'required' => false,
            ])
            ->add('images', FileType::class, [
                'label' => 'Images',
                'required' => false,
                'multiple' => true,
                'mapped' => false,
            ])
            ->add('tags', TextType::class);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function ($event) {
            $data = $event->getData();

            if (empty($data['image'])) {
                $data['image'] = 'images/default.jpg';
            }

            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Gallery::class,
        ]);
    }
}
