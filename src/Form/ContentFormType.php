<?php

namespace App\Form;

use App\DTO\ContentDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;

class ContentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $authors = [
            'bizzaro' => 'bizzaro',
            'LooMis' => 'LooMis',
            'mIZZY' => 'mIZZY',
        ];

        $builder
            ->add('author_name', ChoiceType::class, [
                'choices' => $authors,
            ])
            ->add('title', TextType::class)
            ->add('intro', TextType::class)
            ->add('body', TextareaType::class, [
                'attr' => ['rows' => 10],
                'required' => true
            ]);

            if ($options['rating']) {
                $builder->add('rating', IntegerType::class, [
                    'attr' => ['min' => 0, 'max' => 10]
                ]);
            }         

        $builder  
            ->add('image_main', FileType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('image_search', FileType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('image_detail', FileType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('image', HiddenType::class)
            ->add('image_reference', HiddenType::class)
            ->add('img_main', HiddenType::class)
            ->add('publish_date', TextType::class);

        if ($options['optional_tag']) {
            $builder ->add('optional_tag', TextType::class, ['required' => false]);
        }

        $builder
            ->add('tags', TextType::class)        
            ->add('public', CheckboxType::class, ['required' => false]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function ($event) {
            $data = $event->getData();

            if (empty($data['image'])) {
                $data['image'] = 'images/default.jpg';
            }
    
            if (empty($data['image_reference'])) {
                $data['image_reference'] = 'images/default.jpg';
            }
    
            if (empty($data['img_main'])) {
                $data['img_main'] = 'images/default.jpg';
            }
    
            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ContentDTO::class,
            'validation_groups' => ['contentValidation'],
            'rating' => false,
            'optional_tag' => false,
        ]);
    }
}
