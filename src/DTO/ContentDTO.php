<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class ContentDTO extends BaseDTO
{      
    #[Assert\NotBlank(groups: ['contentValidation'])]
    #[Assert\Length(min: 3, max: 255, groups: ['contentValidation'])]
    #[Assert\Regex(
        pattern: "/^[a-zA-Z0-9\-_\/\.\:\sčČšŠřŘžŽěĚůŮáÁéÉíÍóÓúÚýÝňŇťŤďĎ]+$/u",
        message: "Invalid title format",
        groups: ['contentValidation']
    )]
    private $title;
    

    #[Assert\NotBlank(groups: ['contentValidation'])]
    private $authorName;

    #[Assert\Length(max: 500, groups: ['contentValidation'])]
    private $intro;

    #[Assert\NotBlank(groups: ['contentValidation'])]
    #[Assert\Length(min: 10, groups: ['contentValidation'])]
    private $body;

    #[Assert\Range(min: 0, max: 10, groups: ['contentValidation'])]
    private $rating;

    #[Assert\Length(max: 100, groups: ['contentValidation'])]
    private $optionalTag;

    #[Assert\NotBlank(groups: ['contentValidation'])]
    private $tags;

    #[Assert\Type(type: 'bool', groups: ['contentValidation'])]
    private $public;
    
    private $publicationDate;
    private $relatedContent;
    private $publishDate;
    private $category; 
    private $type;

    public function getAuthorName() {
        return $this->authorName;
    }

    public function setAuthorName($authorName) {
        $this->authorName = $authorName;
        return $this;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function getIntro() {
        return $this->intro;
    }

    public function setIntro($intro) {
        $this->intro = $intro;
        return $this;
    }

    public function getBody() {
        return $this->body;
    }

    public function setBody($body) {
        $this->body = $body;
        return $this;
    }

    public function getRating() {
        return $this->rating;
    }

    public function setRating($rating) {
        $this->rating = $rating;
        return $this;
    }

    public function getPublicationDate() {
        return $this->publicationDate;
    }

    public function setPublicationDate($publicationDate) {
        $this->publicationDate = $publicationDate;
        return $this;
    }

    public function getPublishDate() {
        return $this->publishDate;
    }

    public function setPublishDate($publishDate) {
        $this->publishDate = $publishDate;
        return $this;
    }

    public function getOptionalTag() {
        return $this->optionalTag;
    }

    public function setOptionalTag($optionalTag) {
        $this->optionalTag = $optionalTag;
        return $this;
    }

    public function getTags() {
        return $this->tags;
    }

    public function setTags($tags) {
        $this->tags = $tags;
        return $this;
    }

    public function getPublic(): bool {
        return (bool)$this->public;
    }

    public function setPublic($public) {
        $this->public = $public;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getRelatedContent()
    {
        return $this->relatedContent;
    }

    public function setRelatedContent($relatedContent)
    {
        $this->relatedContent = $relatedContent;

        return $this;
    }
    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }
}
