<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class EventsDTO extends BaseDTO
{
    #[Assert\NotBlank(groups: ['contentValidation'])]
    #[Assert\Length(min: 3, max: 255, groups: ['contentValidation'])]
    #[Assert\Regex(
        pattern: "/^[a-zA-Z0-9\-_\/\.\:\sčČšŠřŘžŽěĚůŮáÁéÉíÍóÓúÚýÝňŇťŤďĎ]+$/u",
        message: "Invalid title format",
        groups: ['contentValidation']
    )]
    private $title;
    

    #[Assert\NotBlank(groups: ['contentValidation'])]
    #[Assert\Length(min: 10, groups: ['contentValidation'])]
    private $text;

    #[Assert\NotBlank(groups: ['contentValidation'])]
    private $tags;

    #[Assert\NotBlank(groups: ['contentValidation'])]
    #[Assert\Length(max: 100, groups: ['contentValidation'])]
    private $venue;

    #[Assert\NotBlank(groups: ['contentValidation'])]
    #[Assert\Length(max: 100, groups: ['contentValidation'])]
    private $time;

    private $publicationDate;
    private $relatedContent;
    private $publishDate;
    private $eventType;
    private $category; 
    private $eventEnd;
    private $date;
    private $type;
    private $body;

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function getPublicationDate() {
        return $this->publicationDate;
    }

    public function setPublicationDate($publicationDate) {
        $this->publicationDate = $publicationDate;
        return $this;
    }

    public function getPublishDate() {
        return $this->publishDate;
    }

    public function setPublishDate($publishDate) {
        $this->publishDate = $publishDate;
        return $this;
    }
    public function getTags() {
        return $this->tags;
    }

    public function setTags($tags) {
        $this->tags = $tags;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getRelatedContent()
    {
        return $this->relatedContent;
    }

    public function setRelatedContent($relatedContent)
    {
        $this->relatedContent = $relatedContent;

        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    public function getVenue()
    {
        return $this->venue;
    }

    public function setVenue($venue)
    {
        $this->venue = $venue;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    public function getEventType()
    {
        return $this->eventType;
    }

    public function setEventType($eventType)
    {
        $this->eventType = $eventType;

        return $this;
    }

    public function getEventEnd()
    {
        return $this->eventEnd;
    }

    public function setEventEnd($eventEnd)
    {
        $this->eventEnd = $eventEnd;

        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }
}
