<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class BaseDTO
{    
    private $id;
    private $contentobject_id;

    #[Assert\Regex(
        pattern: "/^[a-zA-Z0-9\-_\/\.\:]+$/",
        message: "Invalid URL format",
        groups: ['contentValidation']
    )]
    private $url_path;
    
    #[Assert\Regex(
        pattern: "/^images\\/[a-zA-Z0-9-_\\/]+\\.(jpg|jpeg|png|gif)$/",
        message: "Invalid image file path",
        groups: ['contentValidation']
    )]
    private $image;

    #[Assert\Regex(
        pattern: "/^images\\/[a-zA-Z0-9-_\\/]+\\.(jpg|jpeg|png|gif)$/",
        message: "Invalid image file path",
        groups: ['contentValidation']
    )]
    private $imageReference;

    #[Assert\Regex(
        pattern: "/^images\\/[a-zA-Z0-9-_\\/]+\\.(jpg|jpeg|png|gif)$/",
        message: "Invalid image file path",
        groups: ['contentValidation']
    )]
    private $imgMain;

    public function getImageReference()
    {
        return $this->imageReference;
    }

    public function setImageReference($imageReference)
    {
        $this->imageReference = $imageReference;

        return $this;
    }

    public function getImgMain()
    {
        return $this->imgMain;
    }

    public function setImgMain($imgMain)
    {
        $this->imgMain = $imgMain;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getContentobject_id()
    {
        return $this->contentobject_id;
    }

    public function setContentobject_id($contentobject_id)
    {
        $this->contentobject_id = $contentobject_id;

        return $this;
    }

    public function getUrl_path()
    {
        return $this->url_path;
    }

    public function setUrl_path($url_path)
    {
        $this->url_path = $url_path;

        return $this;
    }
}