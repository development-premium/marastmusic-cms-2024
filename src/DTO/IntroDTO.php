<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class IntroDTO
{
    #[Assert\NotBlank(message: "The URL cannot be blank.", groups: ['contentValidation'])]
    #[Assert\Url(message: "The URL is not a valid URL.", groups: ['contentValidation'])]
    #[Assert\Length(
        max: 255,
        maxMessage: "The URL cannot be longer than 255 characters.",
        groups: ['contentValidation']
    )]    
    private $url;

    #[Assert\Regex(
        pattern: "/^images\\/([a-zA-Z0-9-_\\/]+)\\.(jpg|jpeg|png|gif)$/i",
        message: "Invalid image file path",
        groups: ['contentValidation']
    )]
    private $image;

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }
}
