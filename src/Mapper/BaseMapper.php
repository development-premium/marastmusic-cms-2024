<?php

namespace App\Mapper;

class BaseMapper
{
    private const TYPE_NEWS = 'novinka';
    private const TYPE_INTERVIEWS = 'rozhovor';
    private const TYPE_REPORTS = 'report';
    private const TYPE_REVIEWS = 'recenze';
    private const TYPE_ARTICLES = 'article';
    private const TYPE_EVENTS = 'article';

    private const CATEGORY_ABLUMS = 'Alba měsíce';
    private const CATEGORY_OTHERS = 'Ostatní';

    protected function getType($entity) {
        switch ($entity::class) {
            case 'App\Entity\News':
                return self::TYPE_NEWS;
                break;        
            case 'App\Entity\Interviews':
                return self::TYPE_INTERVIEWS;
                break;
            case 'App\Entity\Reports':
                return self::TYPE_REPORTS;
                break;
            case 'App\Entity\Reviews':
                return self::TYPE_REVIEWS;
                break;
            case 'App\Entity\Articles':
                return self::TYPE_ARTICLES;
                break;
            case 'App\Entity\Events':
                return self::TYPE_EVENTS;
                break;
        };
    }

    protected function getCategory($type) {
        switch ($type) {
            case 'album':
                return self::CATEGORY_ABLUMS;
                break;        
            case 'article':
                return self::CATEGORY_OTHERS;
                break;
        };
    }
}