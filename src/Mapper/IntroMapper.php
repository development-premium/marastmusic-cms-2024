<?php

namespace App\Mapper;

class IntroMapper
{
    public function map($introDTO, $data, $entity, $path): object
    {
        $data = reset($data);
        $date = new \DateTime($data['publish_date']);

        $entity->setUrlPath($path);
        $entity->setTitle($data['title']);
        $entity->setAuthorName($this->getAuthorName($data['author_name']));
        $entity->setType($data['type']);
        $entity->setCommentsCount($data['comments_count']);
        $entity->setPublishDate($date);

        if ($introDTO->getImage() === null) {
            $entity->setImage($data['image_reference']);
        } else {
            $entity->setImage($introDTO->getImage());
        }

        return $entity;
    }

    private function getAuthorName($authorName): string
    {
        if (strpos($authorName, '<name>') !== false) {
            $xml = new \SimpleXMLElement("<root>".$authorName."</root>");
            $name = (string)$xml->name;

            return $name;
        } else {
            return $authorName;
        }
    }
}

