<?php

namespace App\Mapper;

use App\DTO\ContentDTO;
use DateTime;

class ContentMapper extends BaseMapper implements ContentMapperInterface
{
    private $type;

    public function __construct($type = null) {
        $this->type = $type;
    }

    public function mapToEntity($dto, $entity): object
    {
        if (!$dto instanceof ContentDTO) {
            throw new \InvalidArgumentException('Expected ContentDTO');
        }

        if ($entity->getPublishDate() === null) {
            $publishDate = DateTime::createFromFormat('Y-m-d H:i', $dto->getPublishDate());
        } else {
            $publishDate = $entity->getPublishDate();
        }

        $entity
               ->setTitle($dto->getTitle())
               ->setAuthorName($dto->getAuthorName())
               ->setIntro($dto->getIntro())
               ->setBody($dto->getBody())
               ->setImage($dto->getImage())
               ->setImageReference($dto->getImageReference())
               ->setImgMain($dto->getImgMain())
               ->setTags($dto->getTags())
               ->setPublic($dto->getPublic() ? 1 : 0)
               ->setPublishDate($publishDate)
               ->setType($this->getType($entity));

        if (method_exists($entity, 'setPublicationDate')) {
            $entity->setPublicationDate(time());
        }
        if (method_exists($entity, 'setOptionalTag')) {
            $entity->setOptionalTag($dto->getOptionalTag());
        }
        if (method_exists($entity, 'setRating')) {
            $entity->setRating($dto->getRating());
        }

        if ($this->type !== null) {
            $category = $this->getCategory($this->type);
            $entity->setCategory($category);
        }

        return $entity;
    }
    
    public function mapToDTO($entity): object
    {        
        $dto = new ContentDTO();
        
        $dto
            ->setTitle($entity->getTitle())
            ->setAuthorName($entity->getAuthorName())
            ->setIntro($entity->getIntro())
            ->setBody($entity->getBody())
            ->setImage($entity->getImage())
            ->setImageReference($entity->getImageReference())
            ->setImgMain($entity->getImgMain())
            ->setTags($entity->getTags())
            ->setPublic($entity->getPublic() == 1)
            ->setPublishDate($entity->getPublishDate())
            ->setType($entity->getType());

        if (method_exists($entity, 'setPublicationDate')) {
            $dto->setPublicationDate($entity->getPublicationDate());
        }
        if (method_exists($entity, 'setOptionalTag')) {
            $dto->setOptionalTag($entity->getOptionalTag());
        }
        if (method_exists($entity, 'setRating')) {
            $dto->setRating($entity->getRating());
        }

        if ($this->type !== null) {
            $category = $this->getCategory($this->type);
            $dto->setCategory($category);
        }

        return $dto;
    }
}
