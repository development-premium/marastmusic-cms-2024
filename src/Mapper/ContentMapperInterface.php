<?php 

namespace App\Mapper;

interface ContentMapperInterface
{
    public function mapToEntity($dto, $entity): object;
    public function mapToDTO($entity): object;
}
