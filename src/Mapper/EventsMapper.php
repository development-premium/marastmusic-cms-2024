<?php

namespace App\Mapper;

use App\DTO\EventsDTO;
use DateTime;

class EventsMapper extends BaseMapper implements ContentMapperInterface
{
    public function mapToEntity($dto, $entity): object
    {
        if (!$dto instanceof EventsDTO) {
            throw new \InvalidArgumentException('Expected EventsDTO');
        }
       
        if ($dto->getEventEnd() !== null) {
            $multidayString = date("d.m", $dto->getDate()->getTimestamp()) . " - " . date("d.m", $dto->getEventEnd()->getTimestamp());
        } else {
            $multidayString = null;
        }

        $dateTime = new DateTime();
        $dateTime->setTimestamp($dto->getDate()->getTimestamp());

        $entity
            ->setTitle($dto->getTitle())
            ->setText($dto->getText())
            ->setImage($dto->getImage())
            ->setImageReference($dto->getImageReference())
            ->setImgMain($dto->getImgMain())
            ->setTags($dto->getTags())
            ->setPublicationDate(time())
            ->setVenue($dto->getVenue())
            ->setDate($dateTime)
            ->setTime($dto->getTime())
            ->setEventType($dto->getEventType())
            ->setMultidayString($multidayString)
            ->setType('event');

        return $entity;
    }
    
    public function mapToDTO($entity): object
    {        
        $dto = new EventsDTO();

        $dto
            ->setTitle($entity->getTitle())
            ->setText($entity->getText())
            ->setImage($entity->getImage())
            ->setImageReference($entity->getImageReference())
            ->setImgMain($entity->getImgMain())
            ->setTags($entity->getTags())
            ->setPublicationDate(time())
            ->setVenue($entity->getVenue())
            ->setDate($entity->getDate())
            ->setTime($entity->getTime())
            ->setEventType($entity->getEventType())
            ->setType('event');
        
        return $dto;
    }
}
