<?php

namespace App\Service\Factory;

use App\Entity\Articles;
use App\Entity\Events;
use App\Entity\Interviews;
use App\Entity\News;
use App\Entity\Reports;
use App\Entity\Reviews;
use App\Service\Factory\BaseContentFactory;

class EntityFactory extends BaseContentFactory
{
    public function createEntity(string $type): object
    {
        switch ($type) {
            case self::TYPE_NEWS:
                return new News();
            case self::TYPE_INTERVIEWS:
                return new Interviews();
            case self::TYPE_REPORTS:
                return new Reports();
            case self::TYPE_REVIEWS:
                return new Reviews();
            case self::TYPE_ALBUMS:
                return new Articles();
            case self::TYPE_OTHERS:
                return new Articles();
            case self::TYPE_EVENTS:
                return new Events();

            default:
                throw new \InvalidArgumentException("Unknown entity type: {$type}");
        }
    }
}
