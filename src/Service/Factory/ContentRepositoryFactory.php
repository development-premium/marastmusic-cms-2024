<?php 

namespace App\Service\Factory;

use App\Entity\Articles;
use App\Entity\Events;
use App\Entity\Interviews;
use App\Entity\News;
use App\Entity\Reports;
use App\Entity\Reviews;
use App\Service\Factory\BaseContentFactory;
use Doctrine\ORM\EntityManagerInterface;

class ContentRepositoryFactory extends BaseContentFactory
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(string $type)
    {
        switch ($type) {
            case self::TYPE_NEWS:
                return $this->entityManager->getRepository(News::class);
            case self::TYPE_INTERVIEWS:
                return $this->entityManager->getRepository(Interviews::class);
            case self::TYPE_REPORTS:
                return $this->entityManager->getRepository(Reports::class);
            case self::TYPE_REVIEWS:
                return $this->entityManager->getRepository(Reviews::class);   
            case self::TYPE_ALBUMS:
                return $this->entityManager->getRepository(Articles::class);    
            case self::TYPE_OTHERS:
                return $this->entityManager->getRepository(Articles::class);   
            case self::TYPE_EVENTS:
                return $this->entityManager->getRepository(Events::class);    

            default:
                throw new \InvalidArgumentException("Unknown repository type: $type");
        }
    }
}
