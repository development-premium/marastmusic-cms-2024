<?php 

namespace App\Service\Factory;

use App\Service\Factory\BaseContentFactory;
use App\Mapper\ContentMapper;
use App\Mapper\ContentMapperInterface;
use App\Mapper\EventsMapper;

class ContentMapperFactory extends BaseContentFactory
{
    public function getMapper($type): ContentMapperInterface
    {
        switch ($type) {
            case self::TYPE_NEWS:
                return new ContentMapper();
            case self::TYPE_INTERVIEWS:
                return new ContentMapper();
            case self::TYPE_REPORTS:
                return new ContentMapper();
            case self::TYPE_REVIEWS:
                return new ContentMapper();
            case self::TYPE_ALBUMS:
                return new ContentMapper($type);
            case self::TYPE_OTHERS:
                return new ContentMapper($type);
            case self::TYPE_EVENTS:
                return new EventsMapper();
        }
        
        throw new \InvalidArgumentException("Unknown type: $type");
    }
}
