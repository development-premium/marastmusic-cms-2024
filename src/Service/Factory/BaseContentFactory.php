<?php

namespace App\Service\Factory;

abstract class BaseContentFactory
{
    protected const TYPE_NEWS = 'novinka';
    protected const TYPE_INTERVIEWS = 'rozhovor';
    protected const TYPE_REPORTS = 'report';
    protected const TYPE_REVIEWS = 'recenze';
    protected const TYPE_ALBUMS = 'album';
    protected const TYPE_OTHERS = 'article';
    protected const TYPE_EVENTS = 'event';
}
