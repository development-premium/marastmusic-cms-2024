<?php 

namespace App\Service\Factory;

use App\DTO\ContentDTO;
use App\DTO\EventsDTO;
use App\Service\Factory\BaseContentFactory;


class DTOFactory extends BaseContentFactory
{
    public function getDTO($type): object
    {
        switch ($type) {
            case self::TYPE_NEWS:
            case self::TYPE_INTERVIEWS:
            case self::TYPE_REPORTS:
            case self::TYPE_REVIEWS:
            case self::TYPE_ALBUMS:
            case self::TYPE_OTHERS:
                return new ContentDTO();
            case self::TYPE_EVENTS:
                return new EventsDTO();
        }
    }
}
