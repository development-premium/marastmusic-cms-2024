<?php 

namespace App\Service;

use App\Repository\ContentRepository;
use App\Service\Factory\ContentRepositoryFactory;
use Psr\Log\LoggerInterface;

class OverviewService
{
    private $repositoryFactory;
    private $contentRepository;
    private $logger;

    public function __construct(
        ContentRepositoryFactory $repositoryFactory,
        ContentRepository $contentRepository,
        LoggerInterface $logger,
    ) {
        $this->repositoryFactory = $repositoryFactory;
        $this->contentRepository = $contentRepository;
        $this->logger = $logger;
    }

    public function getList($type, $search): object
    {
        try {
            $repository = $this->repositoryFactory->create($type);
            return $repository->getCmsListQueryBuilder($search);
        } catch (\Exception $e) {
            $this->logger->error('Error: ' . $e->getMessage());
        }       
    }

    public function parseType($title): string
    {
        switch ($title) {
            case 'novinka':
                return 'novinky';
                break;
            case 'rozhovor':
                return 'rozhovory';
                break;
            case 'report':
                return 'reporty';
                break;
            default:
                return $title;
                break;
        }
    }

    public function getLastEntries($count = 10) {
        return $this->contentRepository->findLastEntries(10);
    }
}