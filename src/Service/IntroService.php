<?php 

namespace App\Service;

use App\DTO\IntroDTO;
use App\Entity\Intro;
use App\Mapper\IntroMapper;
use App\Repository\IntroRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class IntroService
{
    private $introRepository;
    private $introMapper;
    private $entityManager;
    private $logger;

    public function __construct(
        IntroRepository $introRepository,
        IntroMapper $introMapper,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
    ) {
        $this->introRepository = $introRepository;
        $this->introMapper = $introMapper;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function updateItem(IntroDTO $introDTO, int $id): Response
    {
        try {
            $intro = $this->entityManager->getRepository(Intro::class)->find($id);

            $parsedUrl = parse_url($introDTO->getUrl());
            $path = $parsedUrl['path'] ?? '';

            if (substr($path, 0, 1) === '/') {
                $path = substr($path, 1);
            }

            $data = $this->introRepository->getDataByUrl($path); 
            $entity = $this->introMapper->map($introDTO, $data, $intro, $path);

            $this->introRepository->save($entity);

            return new Response('Success');
        } catch (\Exception $e) {
            $this->logger->error('Error: ' . $e->getMessage());
            throw new \Exception($e->getMessage());
        }        
    }
}