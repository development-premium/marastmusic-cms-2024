<?php 

namespace App\Service;

use App\Service\Factory\EntityFactory;
use App\Repository\ContentRepository;
use App\Service\Factory\ContentRepositoryFactory;
use App\Service\Factory\ContentMapperFactory;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ContentService
{
    private $mapperFactory;
    private $contentRepository;
    private $entityManager;
    private $contentRepositoryFactory;
    private $entityFactory;
    private $logger;

    public function __construct(
        ContentMapperFactory $mapperFactory,
        ContentRepository $contentRepository,
        EntityManagerInterface $entityManager,
        ContentRepositoryFactory $contentRepositoryFactory,
        EntityFactory $entityFactory,
        LoggerInterface $logger,
    ) {
        $this->mapperFactory = $mapperFactory;
        $this->contentRepository = $contentRepository;
        $this->entityManager = $entityManager;
        $this->contentRepositoryFactory = $contentRepositoryFactory;
        $this->entityFactory = $entityFactory;
        $this->logger = $logger;
    }

    public function saveData($dto, $type): JsonResponse
    {
        try {
            $entity = $this->entityFactory->createEntity($type);
            $mapper = $this->mapperFactory->getMapper($type);
            
            $this->mapAndSave($mapper, $dto, $entity);
            
            return new JsonResponse([
                'status' => 'success',
                'message' => 'Data saved successfully',
                'data' => [
                    'id' => $entity->getId(),
                ],
            ]);
        } catch (\Exception $e) {
            return new JsonResponse([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function updateEntityFromDTO(int $id, $dto, string $type): JsonResponse
    {
        try {
            $entityToUpdate = $this->contentRepositoryFactory->create($type)->find($id);
            $mapper = $this->mapperFactory->getMapper($type);

            $this->mapAndSave($mapper, $dto, $entityToUpdate, 'update');

            return new JsonResponse([
                'status' => 'success',
                'message' => 'Data saved successfully',
                'data' => [
                    'id' => $entityToUpdate->getId(),
                ],
            ]);
        } catch (\Exception $e) {
            return new JsonResponse([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getContentById($type, $id): object
    {
        try {
            $repository = $this->contentRepositoryFactory->create($type);
            $entity = $repository->find($id);

            $mapper = $this->mapperFactory->getMapper($type);
            $DTO = $mapper->mapToDTO($entity);

            return $DTO;        
        } catch (\Exception $e) {
            $this->logger->error('Error: ' . $e->getMessage());
        }            
    }

    public function replaceLetters($string): string 
    {        
        $lettersList = [
            'á' => 'a', 'č' => 'c', 'ď' => 'd', 
            'é' => 'e', 'ě' => 'e', 'í' => 'i', 
            'ň' => 'n', 'ó' => 'o', 'ř' => 'r', 
            'š' => 's', 'ť' => 't', 'ú' => 'u', 
            'ů' => 'u', 'ý' => 'y', 'ž' => 'z',
            'Á' => 'A', 'Č' => 'C', 'Ď' => 'D', 
            'É' => 'E', 'Ě' => 'E', 'Í' => 'I', 
            'Ň' => 'N', 'Ó' => 'O', 'Ř' => 'R', 
            'Š' => 'S', 'Ť' => 'T', 'Ú' => 'U', 
            'Ů' => 'U', 'Ý' => 'Y', 'Ž' => 'Z'
        ];
        
        return strtr($string, $lettersList);
    }

    public function parseType($title): string
    {
        switch ($title) {
            case 'novinka':
                return 'novinky';
                break;
            case 'rozhovor':
                return 'rozhovory';
                break;
            case 'report':
                return 'reporty';
                break;
            default:
                return $title;
                break;
        }
    }

    public function generateContentObjectId(): string
    {
        return substr(time(), 1, 10) . rand(10,99);
    }

    public function generateUrlPath($type, $title, $folder = ''): string
    {
        $type = $this->parseType($type);
        $title = str_replace("/", "_", $title);
        $normalizedTitle = $this->replaceLetters($title);
        $titleLowerCase = strtolower($normalizedTitle);
        $url_path = preg_replace('/[%#,+() ]/', '_', $titleLowerCase);
        return $type . $folder . "/" . $url_path;
    }

    public function getRelatedContentByTags(string $tags): string 
    {
        return $this->contentRepository->getRelatedContent($tags);
    }

    private function mapAndSave($mapper, $dto, $entity, $action = 'save'): void
    {
        $entity = $mapper->mapToEntity($dto, $entity);
        $type = $this->getTypeForUrl($entity->getType());

        switch ($action) {
            case 'save':
                $entity->setContentObjectId($this->generateContentObjectId());
                $entity->setRelatedContent($this->contentRepository->getRelatedContent($entity->getTags()));
                $entity->setIsRedesignVersion(1);
                $entity->setUrlPath($this->generateUrlPath($type, $entity->getTitle()));
                $entity->setAdded(time());
            break;
            
            case 'update':
                $entity->setUrlPath($this->generateUrlPath($type, $entity->getTitle()));
                $entity->setAdded(time());
            break;
        }

        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    private function getTypeForUrl($type): string 
    {
        switch ($type) {
            case 'report':
                return 'clanky';
                break;

            case 'rozhovor':
                return 'clanky';
                break;    
                
            case 'article':
                return 'clanky';
                break;   

            case 'event':
                return 'akce';
                break;    

            default:
                return $type;
                break;
        }
    }    
}