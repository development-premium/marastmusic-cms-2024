<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('extract_name', [$this, 'extractName']),
            new TwigFilter('capitalizeLowercase', [$this, 'capitalizeLowercaseFilter']),
        ];
    }

    public function extractName($string)
    {
        if (preg_match('/<name>(.*?)<\/name>/', $string, $matches)) {
            return $matches[1];
        }

        return $string;
    }

    public function capitalizeLowercaseFilter($value)
    {
        return ucfirst(strtolower($value));
    }
}
