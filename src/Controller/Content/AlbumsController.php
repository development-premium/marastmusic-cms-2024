<?php

namespace App\Controller\Content;

use App\Controller\BaseController;
use App\Service\ContentService;
use App\Service\OverviewService;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AlbumsController extends BaseController
{
    private const TYPE = 'album';
    private const EDIT_ROUTE = 'app_albums_edit';
    private const UPDATE_ROUTE = 'app_album_update';

    public function __construct(
        ValidatorInterface $validator,
        ContentService $contentService,
        OverviewService $overviewService,
        LoggerInterface $logger,
    ) {
        parent::__construct($validator, $contentService, $overviewService, $logger);
    }

    #[Route('/album', name: 'app_album')]
    public function index(Request $request): Response
    {
        return $this->saveData($request, self::UPDATE_ROUTE, self::TYPE);
    }

    #[Route('/album/update/{id}', name: self::UPDATE_ROUTE)]
    public function update(Request $request, $id): Response
    {
        return $this->updateData($request, $id, self::TYPE, self::UPDATE_ROUTE);
    }

    #[Route('/alba-editace', name: self::EDIT_ROUTE)]
    public function list(Request $request, PaginatorInterface $paginator): Response
    {
        return $this->renderList($request, $paginator, self::TYPE, self::EDIT_ROUTE, self::UPDATE_ROUTE);
    }
}
