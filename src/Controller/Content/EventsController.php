<?php

namespace App\Controller\Content;

use App\Controller\BaseController;
use App\Service\ContentService;
use App\Service\OverviewService;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EventsController extends BaseController
{
    private const TYPE = 'event';
    private const EDIT_ROUTE = 'app_events_edit';
    private const UPDATE_ROUTE = 'app_event_update';

    public function __construct(
        ValidatorInterface $validator,
        ContentService $contentService,
        OverviewService $overviewService,
        LoggerInterface $logger,
    ) {
        parent::__construct($validator, $contentService, $overviewService, $logger);
    }

    #[Route('/akce', name: 'app_event')]
    public function index(Request $request): Response
    {
        return $this->saveData($request, self::UPDATE_ROUTE, self::TYPE);
    }

    #[Route('/akce/update/{id}', name: self::UPDATE_ROUTE)]
    public function update(Request $request, $id): Response
    {
        return $this->updateData($request, $id, self::TYPE, self::UPDATE_ROUTE);
    }

    #[Route('/akce-editace', name: self::EDIT_ROUTE)]
    public function list(Request $request, PaginatorInterface $paginator): Response
    {
        return $this->renderList($request, $paginator, self::TYPE, self::EDIT_ROUTE, self::UPDATE_ROUTE);
    }
}
