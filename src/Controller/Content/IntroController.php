<?php

namespace App\Controller\Content;

use App\Controller\BaseController;
use App\DTO\IntroDTO;
use App\Entity\Box;
use App\Form\BoxFormType;
use App\Form\IntroFormType;
use App\Service\IntroService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IntroController extends BaseController
{
    private const TITLES = [
        1 => 'Hlavní banner',
        2 => 'Malý horní banner',
        3 => 'Malý dolní banner',
        4 => 'Box pod bannerem',
    ];

    private $introService;
    private $logger;

    public function __construct(IntroService $introService, LoggerInterface $logger) {
        $this->introService = $introService;
        $this->logger = $logger;
    }

    #[Route('/intro/{id}', name: 'app_intro')]
    public function index(Request $request, $id): Response
    {
        $introDTO = new IntroDTO;
        $form = $this->createForm(IntroFormType::class, $introDTO);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $this->introService->updateItem($introDTO, $id);                    
            } catch (\Exception $e) {
                $this->addFlash('error', $e->getMessage());
                $this->logger->error('Error: ' . $e->getMessage());
                return $this->render('error.html.twig');
            }
            return $this->redirectToRoute('app_home');
        }

        return $this->render('form/introForm.html.twig', [
            'title' => self::TITLES[$id],
            'form' => $form->createView(),
            'introFlag' => true,
            'imageType' => $id,
        ]);   
    }

    #[Route('/box', name: 'app_box')]
    public function box(Request $request, EntityManagerInterface $entityManager): Response
    {
        try {
            $box = new Box;
            $form = $this->createForm(BoxFormType::class, $box);
    
            $form->handleRequest($request);
    
            if ($form->isSubmitted() && $form->isValid()) {
    
                $entityManager->persist($box);
                $entityManager->flush();            
    
                return $this->redirectToRoute('app_home');
            }

            return $this->render('form/boxForm.html.twig', [
                'title' => self::TITLES[4],
                'form' => $form->createView(),
                'introFlag' => true,
                'imageType' => 3,
            ]);

        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
            $this->logger->error('Error: ' . $e->getMessage());
            return $this->render('error.html.twig');
        }
    }
}