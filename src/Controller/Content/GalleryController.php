<?php

namespace App\Controller\Content;

use App\Controller\BaseController;
use App\Entity\Gallery;
use App\Entity\Images;
use App\Form\GalleryFormType;
use App\Service\ContentService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController extends BaseController
{
    private const TITLE = 'galerie';

    public $entityManager;
    public $contentService;

    public function __construct(
        EntityManagerInterface $entityManager,
        ContentService $contentService,

    ) {
        $this->entityManager = $entityManager;
        $this->contentService = $contentService;
    }

    #[Route('/galerie', name: 'app_gallery')]
    public function createGallery(Request $request, KernelInterface $kernel): Response
    {        
        $gallery = new Gallery();
        $form = $this->createForm(GalleryFormType::class, $gallery);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->saveGallery($gallery);
        
            $uploadedImages = $form->get('images')->getData();
            
            foreach ($uploadedImages as $uploadedImage) {
                $image = new Images();
                
                $newImageName = md5(uniqid()).'.'.$uploadedImage->guessExtension();                
                $imagesFolder = dirname($kernel->getProjectDir()) . '/marastmusic/public/images/';

                $folderName = preg_replace('/[%#,+() ]/', '_', strtolower($this->contentService->replaceLetters(str_replace("/", "_", $gallery->getTitle()))));
                $galleryFolder = $imagesFolder . self::TITLE . '/' . $folderName;

                $imagePath = 'images/' . self::TITLE . '/' . $folderName . '/' . $uploadedImage->getClientOriginalName();

                $this->saveImage($image, $gallery, $uploadedImage->getClientOriginalName(), $imagePath);

                if (!file_exists($galleryFolder)) {
                    mkdir($galleryFolder, 0755, true);
                }

                $uploadedImage->move(
                    $galleryFolder,
                    $newImageName
                );
            }

            $this->redirectToRoute('app_home');
        }
            
        return $this->render('form/galleryForm.html.twig', [
            'form' => $form->createView(),
            'title' => self::TITLE,
        ]);
    }

    private function saveGallery($gallery): void 
    {
        $gallery->setContentobjectId($this->contentService->generateContentObjectId());
        $gallery->setUrlPath($this->contentService->generateUrlPath(self::TITLE, $gallery->getTitle()));
        $gallery->setType(self::TITLE);
        $gallery->setAdded(time());
        $gallery->setRelatedContent($this->contentService->getRelatedContentByTags($gallery->getTags()));
        $gallery->setImageReference($gallery->getImage());

        $this->entityManager->persist($gallery);
        $this->entityManager->flush();
    }

    private function saveImage($image, $gallery, $upload, $path): void
    {
        $image->setGallery($gallery);
        $image->setName($upload);
        $image->setUrlPath($path);
        $image->setImage($path);
        $image->setImageReference($path);

        $this->entityManager->persist($image);
        $this->entityManager->flush();
    }
}
