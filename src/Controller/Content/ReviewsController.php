<?php

namespace App\Controller\Content;

use App\Controller\BaseController;
use App\Service\ContentService;
use App\Service\OverviewService;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ReviewsController extends BaseController
{
    private const TYPE = 'recenze';
    private const EDIT_ROUTE = 'app_reviews_edit';
    private const UPDATE_ROUTE = 'app_review_update';

    private $defaultOptions;

    public function __construct(
        ValidatorInterface $validator,
        ContentService $contentService,
        OverviewService $overviewService,
        LoggerInterface $logger,
    ) {
        parent::__construct($validator, $contentService, $overviewService, $logger);

        $this->defaultOptions = ['rating' => true];
    }

    #[Route('/recenze', name: 'app_review')]
    public function index(Request $request): Response
    {
        return $this->saveData($request, self::UPDATE_ROUTE, self::TYPE, $this->defaultOptions);
    }

    #[Route('/recenze/update/{id}', name: self::UPDATE_ROUTE)]
    public function update(Request $request, $id): Response
    {
        return $this->updateData($request, $id, self::TYPE, self::UPDATE_ROUTE, $this->defaultOptions);
    }

    #[Route('/recenze-editace', name: self::EDIT_ROUTE)]
    public function list(Request $request, PaginatorInterface $paginator): Response
    {
        return $this->renderList($request, $paginator, self::TYPE, self::EDIT_ROUTE, self::UPDATE_ROUTE);
    }
}
