<?php

namespace App\Controller;

use App\Service\OverviewService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(OverviewService $overviewService, Request $request): Response
    {
        $host = $request->getHost();
        $parts = explode('.', $host);
        $domain = count($parts) > 2 ? implode('.', array_slice($parts, -2)) : $host;

        $lastEntries = $overviewService->getLastEntries(10);

        return $this->render('home/index.html.twig', [
            'lastEntries' => $lastEntries,
            'domain' => $domain,
        ]);
    }

    #[Route('/cache', name: 'app_cache')]
    public function cache(): Response
    {
        $cache = new FilesystemAdapter();
        $cache->clear();

        return $this->redirectToRoute('app_home');
    }

    #[Route('/error', name: 'app_error')]
    public function error(): Response
    {
        return $this->render('error.html.twig');
    }
}
