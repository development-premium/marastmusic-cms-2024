<?php

namespace App\Controller;

use App\Service\Factory\ContentRepositoryFactory;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class ContentController extends AbstractController
{
    private const IMAGES_FOLDER = '/marastmusic/public/images/';

    private $kernel;
    private $entityManager;
    private $logger;

    public function __construct(
        KernelInterface $kernel,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
    ) {
        $this->kernel = $kernel;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    #[Route('/save-img/{type}', name: 'app_save_img', methods: ['POST'])]
    public function saveImg(Request $request, $type): Response
    {
        try {
            $base64Image = $request->request->get('image');

            if ($base64Image) {
                $imageData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64Image));
                $filename = uniqid() . '.png';

                $marastMusicDir = $this->getImagesFolder();

                $filePath = $marastMusicDir . $type . '/' . $filename;
                file_put_contents($filePath, $imageData);
            }    
            
            return new Response('images/' . $type . '/' . $filename);
            
        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
            $this->logger->error('Error: ' . $e->getMessage());
            return $this->render('error.html.twig');
        }       
    }

    #[Route('/save-form-img', name: 'app_save_form_img')]
    public function saveFormImg(Request $request): Response
    {
        try {
            $uploadedImage = $request->files->get('image');

            if ($uploadedImage && $uploadedImage->isValid()) {
                $marastMusicDir = $this->getImagesFolder() . 'cms-images/';
                $cmsDir = $this->kernel->getProjectDir() . '/public/images/cms-images/';

                $newImgName = uniqid() . '.' . $uploadedImage->guessExtension();
                $uploadedImage->move($marastMusicDir, $newImgName);

                $sourcePath = $marastMusicDir . $newImgName;
                $cmsPath = $cmsDir . $newImgName;

                copy($sourcePath, $cmsPath);
            }
            
            return new Response($newImgName);

        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
            $this->logger->error('Error: ' . $e->getMessage());
            return $this->render('error.html.twig');
        }      
        
    }

    #[Route('/delete-item', name: 'app_delete_item')]
    public function detele(Request $request, ContentRepositoryFactory $repositoryFactory): Response
    {
        try {
            $id = $request->query->get('id');
            $type = $request->query->get('type');
            $route = $request->query->get('route');
    
            $repository = $repositoryFactory->create($type);
            $entity = $repository->find($id);
    
            if ($entity) {
                $this->entityManager->remove($entity);
                $this->entityManager->flush();
            }
    
            return $this->redirectToRoute($route);

        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
            $this->logger->error('Error: ' . $e->getMessage());
            return $this->render('error.html.twig');
        }            
    }

    private function getImagesFolder(): string
    {
        $projectDir = $this->kernel->getProjectDir();
        $parentDir = dirname($projectDir);
        return $parentDir . self::IMAGES_FOLDER;
    }
}

