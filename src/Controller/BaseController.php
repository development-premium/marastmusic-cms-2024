<?php

namespace App\Controller;

use App\Form\ContentFormType;
use App\Form\EventFormType;
use App\Service\ContentService;
use App\Service\Factory\DTOFactory;
use App\Service\OverviewService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseController extends AbstractController
{
    private $validator;
    private $contentService;
    private $overviewService;
    private $logger;

    public function __construct(
        ValidatorInterface $validator,
        ContentService $contentService,
        OverviewService $overviewService,
        LoggerInterface $logger,

    ) {
        $this->validator = $validator;
        $this->contentService = $contentService;
        $this->overviewService = $overviewService;
        $this->logger = $logger;
    }

    public function saveData(Request $request, $route, $type, $options = []): Response
    {        
        try {
            $dataDTO = (new DTOFactory())->getDto($type);
            $formType = $this->getFormType($type);

            $form = $this->createForm($formType::class, $dataDTO, $options);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $jsonContent = $this->contentService->saveData($dataDTO, $type);
                $id = json_decode($jsonContent->getContent())->data->id;

                return $this->redirectToRoute($route, ['id' => $id]);
            }

            $errors = $this->validator->validate($dataDTO, null, 'contentValidation');

            if ($form->isSubmitted() && count($errors) !== 0) {
                foreach ($errors as $error) {
                    $this->logger->error(sprintf(
                        'Validation error: %s: %s',
                        $error->getPropertyPath(),
                        $error->getMessage()
                    ));
                }
            }   

            return $this->render($this->getTemplate($type), [
                'form' => $form->createView(),
                'type' => $type,
                'isUpdate' => false,
            ]);        

        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
            $this->logger->error('Error: ' . $e->getMessage());
            return $this->render('error.html.twig');
        }        
    }

    public function updateData(Request $request, $id, $type, $route, $options = []): Response
    {
        try {
            $dataDTO = $this->contentService->getContentById($type, $id);
            $formType = $this->getFormType($type);
           
            $form = $this->createForm($formType::class, $dataDTO, $options);
            $form->handleRequest($request);
    
            if ($form->isSubmitted() && $form->isValid()) {
                $dataDTO->setId($id);
                $jsonContent = $this->contentService->updateEntityFromDTO($id, $dataDTO, $type);
                $id = json_decode($jsonContent->getContent())->data->id;
    
                return $this->redirectToRoute($route, ['id' => $id]);
            }

            $errors = $this->validator->validate($dataDTO, null, 'contentValidation');

            if ($form->isSubmitted() && count($errors) !== 0) {
                foreach ($errors as $error) {
                    $this->logger->error(sprintf(
                        'Validation error: %s: %s',
                        $error->getPropertyPath(),
                        $error->getMessage()
                    ));
                }
            }   
    
            return $this->render($this->getTemplate($type), [
                'form' => $form->createView(),
                'type' => $type,
                'isUpdate' => true,
            ]);

        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
            $this->logger->error('Error: ' . $e->getMessage());
            return $this->render('error.html.twig');
        }         
    }

    public function renderList($request, $paginator, $type, $editRoute, $updateRoute): Response
    {
        try {
            $search = $request->query->get('search', '');
            $overview = $this->overviewService->getList($type, $search);

            $currentPage = $request->query->getInt('page', 1);

            $pagination = $paginator->paginate(
                $overview,
                $currentPage,
                10
            );

            $title = $this->overviewService->parseType($type);

            return $this->render('overview.html.twig', [
                'overview' => $overview,
                'type' => $type,
                'title' => $title,
                'pagination' => $pagination,
                'search' => $search,
                'editRoute' => $editRoute,
                'updateRoute' => $updateRoute,
            ]);
            
        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
            $this->logger->error('Error: ' . $e->getMessage());
            return $this->render('error.html.twig');
        }        
    }

    private function getTemplate($type): string
    {
        switch ($type) {
            case 'event':
                return 'form/eventForm.html.twig';
                break;
            
            default:
                return 'form/contentForm.html.twig';
                break;
        }
    }

    private function getFormType($type)
    {
        switch ($type) {
            case 'event':
                return new EventFormType;
                break;
            
            default:
                return new ContentFormType;
                break;
        }
    }    
}
